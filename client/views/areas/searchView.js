var domify = require('domify');

var doT = require('dot');

var searchTemplate= require('../../templates/demo/searchTemplate.html');

var EventEmitter2 = require('eventemitter2').EventEmitter2;

function SearchView(){
    this.fragment = null;
    this.template = doT.compile(searchTemplate, undefined, {});
}
SearchView.prototype = Object.create(EventEmitter2.prototype);
SearchView.prototype.constructor = SearchView;

SearchView.prototype.render = function(searchplaces){

    this.fragment= document.createDocumentFragment();

    this.fragment.appendChild(domify(this.template(searchplaces)));
    this.bindEvent();
    return this.fragment;

};
SearchView.prototype.submitSearch=function(e){
    if (e.preventDefault) e.preventDefault();

    this.emit('searchView.submit',e.currentTarget[0].value);
    return false;
};

SearchView.prototype.segmentSearch=function(e){
    if (e.preventDefault) e.preventDefault();

    this.emit('searchView.segmentclick',e.currentTarget.id);
    return false;
};

SearchView.prototype.installSubmitsSearchViewHandler=function(searchViewHandler, domSelector){
    var view=this;
    var form=this.fragment.querySelector(domSelector);
    if(form.attachEvent) {
        form.attachEvent("submit", function(event){
            searchViewHandler.call(view, event);
        });

    }else {
        form.addEventListener("submit", function (event) {
            searchViewHandler.call(view, event);
        });
    }
};

SearchView.prototype.installSegmentsSearchViewHandler=function(searchViewHandler, domSelector){
    var view=this;
    var dom=this.fragment.querySelectorAll(domSelector);
    for (var i=0;i<dom.length;i++){
        dom[i].addEventListener("click", function (event) {
            searchViewHandler.call(view, event);
        });
    }
};

SearchView.prototype.bindEvent= function(){
    this.handleSubmitSearchEvent();
    this.handleSegmentSearchEvent();
};

SearchView.prototype.handleSubmitSearchEvent= function(){
    this.installSubmitsSearchViewHandler(this.submitSearch,"form#search");
};

SearchView.prototype.handleSegmentSearchEvent= function(){
    this.installSegmentsSearchViewHandler(this.segmentSearch,".segment");
};

module.exports=SearchView;
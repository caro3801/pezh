var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');
var doT = require('dot');

var infoTemplate= require('../../templates/demo/infoTemplate.html');


function InfoAreaView() {
    this.fragment = null;
    this.template = doT.compile(infoTemplate, undefined, {});
}

InfoAreaView.prototype = Object.create(EventEmitter2.prototype);
InfoAreaView.prototype.constructor = InfoAreaView;

InfoAreaView.prototype.render = function(areaInfo) {
    var fragment = document.createDocumentFragment();

    fragment.appendChild(domify(this.template(areaInfo)));
    return fragment;
};

InfoAreaView.prototype.clear = function(){
    var fragment = document.createDocumentFragment();
    fragment.appendChild(domify(this.template()));
    return fragment;
};

module.exports = InfoAreaView;

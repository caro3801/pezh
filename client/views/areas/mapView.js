/**
 * Created by caroline on 01/05/14.
 */
var EventEmitter2 = require('eventemitter2').EventEmitter2;

var parcelStyle = {
    strokeColor: "white",
    strokeOpacity: 1,
    strokeWeight: 1,
    fillColor: "#729DBB",
    fillOpacity: 0.3
};
var parcelStyleSelected = {
    strokeColor: "white",
    strokeOpacity: 1,
    strokeWeight: 1,
    fillColor: "#B1C800",
    fillOpacity: 0.50
};
var parcelStyleOver = {
    strokeColor: "white",
    strokeOpacity: 1,
    strokeWeight: 1,
    fillColor: "#75BDE7",
    fillOpacity: 0.50
};

function MapView(zone) {
    this.domFragment = document.createDocumentFragment();
    this.mapOptions = {
        center: new google.maps.LatLng(zone.latitude,zone.longitude),
        zoom: zone.zoom,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var div = document.createElement('div');
    div.classList.add('map');
    this.map = new google.maps.Map(div);
    this.domFragment.appendChild(div);
}

MapView.prototype = Object.create(EventEmitter2.prototype);
MapView.prototype.constructor = MapView;

MapView.prototype.render = function(areas) {
    for (var key in areas){
        var geojson="";
        geojson=areas[key].frontier;
        this.map.data.addGeoJson({type:"Feature",id:areas[key].id,geometry:JSON.parse(geojson)});
    }
    this.map.data.setStyle(parcelStyle);
    this.bindEvent();
    return this.domFragment;
};

MapView.prototype.centerMapView = function(center){
    this.map.setCenter(new google.maps.LatLng(center.latitude,center.longitude));
};

MapView.prototype.zoomMapView = function(zoom){
    this.map.setZoom(zoom);
};
MapView.prototype.refreshMapView = function() {
    google.maps.event.trigger(this.map, "resize");
    this.map.setCenter(this.mapOptions.center);
    this.map.setZoom(this.mapOptions.zoom);
    this.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
};

MapView.prototype.renderArea = function(area) {
    var style = area.selected ? parcelStyleSelected : parcelStyle;
    var eFeature = this.map.data.getFeatureById(area.id);
    this.map.data.overrideStyle(eFeature, style);
};

MapView.prototype.click = function(event) {
    var eFeature = event.feature;
    this.emit('mapview.click.area',eFeature.getId());
};

MapView.prototype.mouseover = function(event) {
    var eFeature=event.feature;
    this.map.data.overrideStyle(eFeature, parcelStyleOver);
    this.emit('mapview.mouseover.area',eFeature.getId());
};

MapView.prototype.mouseout = function(event) {
    var eFeature=event.feature;
    this.emit('mapview.mouseout.area',eFeature.getId());
};

MapView.prototype.bindEvent = function() {
    var view = this;
    this.map.data.addListener('click', function(event){
        view.click(event);
    });
    this.map.data.addListener('mouseover', function(event){
        view.mouseover(event);
    });
    this.map.data.addListener('mouseout', function(event){
        view.mouseout(event);
    });
};

module.exports = MapView;
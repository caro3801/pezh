var MapView = require("./mapView");
var InfoAreaView = require("./infoAreaView");
var ListAreaView = require("./listAreaView");
var SearchView = require("./searchView");
var RegisterView = require("../register/registerView");
var PageView = require("../pages/pageView");
var Wrapper = require('./../shared/wrapper');
var WrapperGroup = require('./../shared/wrapperGroup');
var demoTemplate = require('../../textes/solutions/mapdemo.html');
var domify = require('domify');

function AreasView(zone, visuId) {
    this.visuId=visuId;
    this.mapWrapper = new Wrapper('.map-wrapper');
    this.mapWrapper.addClass("container_map");
    this.infoAreaWrapper = new Wrapper('.container_map_info');
    this.listAreasWrapper = new Wrapper('.listareas-wrapper');
    this.searchWrapper = new Wrapper('.search-wrapper');
    this.registerWrapper = new Wrapper('.register-wrapper');


    this.mapView = new MapView(zone);
    this.infoAreaView = new InfoAreaView();
    this.listAreaView = new ListAreaView(this.visuId);
    this.searchView=new SearchView();
    this.registerView = new RegisterView();
    this.pageView = new PageView();
    this.separatorWrapper ={domElement: domify('<p style="clear:both;"></p>')};
    var wrappers = [this.mapWrapper, this.infoAreaWrapper,this.separatorWrapper/*,this.searchWrapper*/,this.listAreasWrapper,this.registerWrapper];

    this.wrapperGroup = new WrapperGroup(wrappers);
}

AreasView.prototype.render = function(data) {
    this.mapWrapper.inject(this.mapView.render(data.map));
    this.renderInfoArea();
    this.searchWrapper.inject(this.renderSearch(data.searchplaces));

    this.registerWrapper.inject(this.renderRegister());
    return this.pageView.renderDOM(domify(demoTemplate),this.wrapperGroup.getFragment());
};



AreasView.prototype.renderSearch=function(searchplaces){
   return this.searchView.render(searchplaces) ;
};

AreasView.prototype.renderRegister= function(){
    return this.registerView.render();
};
AreasView.prototype.renderArea = function(area) {
    this.mapView.renderArea(area);
};

AreasView.prototype.renderList = function(areas) {
    this.listAreasWrapper.inject(this.listAreaView.render(areas));
};

AreasView.prototype.renderInfoArea = function(areaInfo) {
    this.infoAreaWrapper.inject(this.infoAreaView.render(areaInfo));
};

AreasView.prototype.clearInfoArea = function() {
    this.infoAreaWrapper.inject(this.infoAreaView.clear());
};

AreasView.prototype.refreshMapView = function() {
    this.mapView.refreshMapView();
};
AreasView.prototype.centerMapView = function(center) {
    this.mapView.centerMapView(center);
};

AreasView.prototype.zoomMapView = function(zoom) {
    this.mapView.zoomMapView(zoom);
};
AreasView.prototype.onMapView = function(event, callBack) {
    this.mapView.on(event, callBack);
};

AreasView.prototype.onListView = function(event, callBack) {
    this.listAreaView.on(event, callBack);
};
AreasView.prototype.onSearchView = function(event, callBack) {
    this.searchView.on(event, callBack);
};

AreasView.prototype.onRegisterView = function(event, callBack) {
    this.registerView.on(event, callBack);
};
module.exports = AreasView;
/**
 * Created by caroline on 01/05/14.
 */
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');

var doT = require('dot');

var listTemplate= require('../../templates/demo/listTemplate.html');

function ListAreaView(visuId) {
    this.visuId=visuId;
    this.template = doT.compile(listTemplate, undefined, {});
    this.fragment = null;
}

ListAreaView.prototype = Object.create(EventEmitter2.prototype);
ListAreaView.prototype.constructor = ListAreaView;


ListAreaView.prototype.render = function(selectedAreas) {
    this.fragment = document.createDocumentFragment();

    var dto = {length: Object.keys(selectedAreas).length, areas: selectedAreas};

    this.fragment.appendChild(domify(this.template(dto)));
    this.bindEvent();
    return this.fragment;
};

ListAreaView.prototype.clickDel=function(event){
    var domObject=event.currentTarget;
    this.emit('listareaview.delete',domObject.id);
};
ListAreaView.prototype.clickResults=function(event){
    this.emit('listareaview.results',this.selectedMuns);
};

ListAreaView.prototype.installAreaListHandler=function(listAreaHandler, domSelector){
    var view=this;
    var buttons=this.fragment.querySelectorAll(domSelector);
    var i=0;
    for (;i<buttons.length;i++){
        buttons[i].addEventListener("click", function(event){
            listAreaHandler.call(view, event);
        });
    }
};

ListAreaView.prototype.bindEvent=function() {
    this.handleResultsEvent();
    this.handleDelEvent();
};

ListAreaView.prototype.handleDelEvent=function(){
    this.installAreaListHandler(this.clickDel,".poub");
};

ListAreaView.prototype.handleResultsEvent=function handleResultsEvent(){
    this.installAreaListHandler(this.clickResults,"#results");
};

module.exports=ListAreaView;
var MenuView = require("./menuView");
var SliderView = require("../home/sliderView");
var domify = require('domify');
var BasketMiniView = require("../basket/basketMiniView");
var Wrapper = require('./wrapper');
var WrapperGroup = require('./wrapperGroup');
var tabsTemplate = require('./../../templates/accueil/tabsTemplate.html');

function HeaderView(data,withSlider) {
    this.withSlider = withSlider;

    this.wrapperHeaderContent = new Wrapper('.content');
    this.tabsWrapper = new Wrapper(".header-onglets");
    this.tabsWrapper.setId("header-onglets");
    this.tabsTemplate=tabsTemplate;
    this.solutions=data;
    this.menuView = new MenuView();
    this.sliderView = new SliderView();
    var wrappers = [this.wrapperHeaderContent];

    this.wrapperGroup = new WrapperGroup(wrappers);

    //this.basketMiniView = new BasketMiniView();

}

HeaderView.prototype.render = function(isHome,nbItem){
    this.wrapperHeaderContent.inject(this.renderMenu());
    if(isHome){

        this.wrapperHeaderContent.append(this.renderTabs());
    }

    return this.wrapperGroup.getFragment();
};

HeaderView.prototype.renderSlider = function(data){
    return this.sliderView.render(this.solutions);
};

HeaderView.prototype.renderImage = function(){
    var headImages = [
        {src:'images/solutions/slide1.jpg',alt:'slide1'},
        {src:'images/solutions/slide2.jpg',alt:'slide2'},
        {src:'images/solutions/slide3.jpg',alt:'slide3'},
        {src:'images/solutions/slide4.jpg',alt:'slide4'},
        {src:'images/solutions/slide5.jpg',alt:'slide5'}
        ];
    var image = headImages[Math.floor(Math.random()*headImages.length)];
    return domify(' <div id="slider-content" style="height: 240px;"><img src="'+image.src+'" alt="'+image.alt+'" class="headImage"/></div>');
   // return this.sliderView.render({image:image});
};
HeaderView.prototype.renderTabs = function(){
    return this.tabsWrapper.inject(domify(this.tabsTemplate));
};
HeaderView.prototype.renderMenu = function(){
    return this.menuView.render();
};

HeaderView.prototype.renderMiniBasket = function(nbItem) {
    this.wrapperMiniBasket.inject(this.basketMiniView.render(nbItem));
};

HeaderView.prototype.onMenuView = function(event, callBack) {
    this.menuView.on(event, callBack);
};

HeaderView.prototype.thanksNewsDisabled = function(){
    this.wrapperHeaderContent.domElement.getElementsByTagName("label")[0].innerText = "Vous recevrez prochainement nos news";
    var elem= this.wrapperHeaderContent.domElement.querySelectorAll('input');
    elem[0].disabled ="disabled";
    elem[1].disabled ="disabled";
    elem[1].style.backgroundColor ="#748490";
    elem[1].style.cursor ="default";
    elem[1].style.textDecoration ="initial";
    elem[1].value ="MERCI";
};
module.exports = HeaderView;
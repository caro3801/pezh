/**
 * Created by caroline on 01/05/14.
 */
"use strict";
var doT = require('dot');
var domify = require('domify');
var templateM = require('./../../templates/autres/menuTemplate.html');
var EventEmitter2 = require('eventemitter2').EventEmitter2;
function MenuView() {
    this.fragment=null;
    this.tmpl=doT.compile(templateM, undefined, {});
}

MenuView.prototype = Object.create(EventEmitter2.prototype);
MenuView.prototype.constructor = MenuView;
MenuView.prototype.render = function menuViewRender() {
    this.fragment= document.createDocumentFragment();
    this.fragment.appendChild(domify(this.tmpl()));
    this.bindEvent();
    return this.fragment;
};
MenuView.prototype.bindEvent=function(){
    this.handleNewsFormEvent();
};


MenuView.prototype.handleNewsFormEvent=function(){
    this.installNewsFormHandler(this.submitNews,"form#newsform");
};
MenuView.prototype.submitNews = function(e){
    if (e.preventDefault) e.preventDefault();

    this.emit('menuview.newssubmit',e.currentTarget[0].value);
};
MenuView.prototype.installNewsFormHandler=function(menuViewHandler, domSelector){
    var view=this;
    var form=this.fragment.querySelector(domSelector);
    if(form.attachEvent) {
        form.attachEvent("submit", function(event){
            menuViewHandler.call(view, event);
        });

    }else {
        form.addEventListener("submit", function (event) {
            menuViewHandler.call(view, event);
        });

    }

};



module.exports = MenuView;
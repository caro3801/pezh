function WrapperGroup(wrappers) {
    this.wrappers = wrappers;
}

WrapperGroup.prototype.getFragment = function() {
    var fragment = document.createDocumentFragment();
    for(var i=0; i<this.wrappers.length; i++) {
        fragment.appendChild(this.wrappers[i].domElement);
    }
    return fragment;
};

module.exports = WrapperGroup;
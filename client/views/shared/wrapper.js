function Wrapper(domSelector) {
    this.domElement = document.querySelector(domSelector);
    if(!this.domElement || this.domElement.className == 'content') {
        this.domElement = document.createElement('div');
        this.domElement.classList.add(domSelector.substring(1));
    }
}
Wrapper.prototype.addClass= function (myclass){
    this.domElement.classList.add(myclass);
};

Wrapper.prototype.setId= function (id){
    this.domElement.id=id;
};
Wrapper.prototype.inject = function(domFragment) {
    this.domElement.innerHTML = "";
    this.domElement.appendChild(domFragment);
    return this.domElement;
};

Wrapper.prototype.append = function(domFragment) {
    this.domElement.appendChild(domFragment);
};

module.exports = Wrapper;
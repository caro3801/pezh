var domify = require('domify');
var doT = require('dot');
var footerViewTemplate = require('./../../templates/autres/footerTemplate.html');
function FooterView(){
    this.template = doT.compile(footerViewTemplate, undefined, {});
}
FooterView.prototype.render = function(active){
    return  domify(this.template(active));
};

FooterView.prototype.setActive = function(idElement){

};
module.exports = FooterView;
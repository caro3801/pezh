function MapWrapper(domSelector) {
    this.domElement = document.querySelector(domSelector);
    this.map = new google.maps.Map(this.domElement);
}

MapWrapper.prototype.inject = function(data) {
    this.map.setData(data);
};

module.exports = MapWrapper;
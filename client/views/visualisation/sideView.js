var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');
var doT = require('dot');
var sideTemplate= require('../../templates/results/sideTemplate.html');

function SideView() {
    this.fragment = null;
    this.template = doT.compile(sideTemplate, undefined, {});
}
SideView.prototype = Object.create(EventEmitter2.prototype);
SideView.prototype.constructor = SideView;

SideView.prototype.render=function render(dlData){

    this.fragment=document.createDocumentFragment();

    this.fragment.appendChild(domify(this.template(dlData)));

    this.bindEvent();
    return this.fragment;
};

SideView.prototype.bindEvent=function(){
    this.handleDownloadEvent();
};


SideView.prototype.installSideViewSubmitHandler=function(sideHandler, domSelector){
    var view=this;
    var form=this.fragment.querySelector(domSelector);
    if(form.attachEvent) {
        form.attachEvent("submit", function(event){
            sideHandler.call(view, event);
        });

    }else {
        form.addEventListener("submit", function (event) {
            sideHandler.call(view, event);
        });

    }

};
SideView.prototype.renderError = function(){
    this.fragment.getElementById("error").html="Cochez au moins une zone";
};
SideView.prototype.submitDownload=function(event){
    event.preventDefault(true);
    var domObject=event.currentTarget;
    this.emit('sideview.downloadSubmit',domObject);
};

SideView.prototype.handleDownloadEvent=function(){
    this.installSideViewSubmitHandler(this.submitDownload,"form#downloadForm");
};

module.exports=SideView;
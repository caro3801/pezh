/**
 * Created by caroline on 01/05/14.
 */
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');

function MapRView(zone) {
    this.domFragment = document.createDocumentFragment();
    this.geoJSONStyles = {
        "past":{
            "fillColor":"#729DBB",
            "strokeColor":"#729DBB",
            "strokeWeight": 0.15,
            "fillOpacity": 0.75,
            "zIndex":1

        },"present":{
            "fillColor":"#EDB52F",
            "strokeColor":"#EDB52F",
            "strokeWeight": 0.2,
            "fillOpacity": 0.75,
            "zIndex":2


        },"frontier":{
            "fillColor":"white",
            "strokeColor":"white",
            "strokeWeight": 1,
            "fillOpacity": 0.0,
            "zIndex":3
        }
    };
    this.mapOptions = {
        center: new google.maps.LatLng(zone.latitude,zone.longitude),
        zoom: zone.zoom,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var div = document.createElement('div');
    div.classList.add('map');
    this.map = new google.maps.Map(div);

    this.domFragment.appendChild(div);



}

MapRView.prototype = Object.create(EventEmitter2.prototype);
MapRView.prototype.constructor = MapRView;


MapRView.prototype.render = function(geojson) {
    var view= this;
    this.map.data.addGeoJson(geojson.frontiersData);

    this.map.data.addGeoJson(geojson.cellsData);

    this.map.data.setStyle(function(feature) {
        var s = view.geoJSONStyles[feature.getProperty('display')];
        return ({
            fillColor: s["fillColor"],
            strokeColor:  s["strokeColor"],
            strokeWeight:  s["strokeWeight"],
            fillOpacity:  s["fillOpacity"],
            zIndex:  s["zIndex"]
        });
    });

    var pastC = geojson.checkedPeriods["past"]?'checked':'';
    var presentC=geojson.checkedPeriods["present"]?'checked':'';
    var checkPeriodsContent = "<label style='float:left;line-height: 27px;' for='presentCheck'>Milieux humides 2013</label>" +
        "<input style='float:left;line-height: 27px; ' type='checkbox'  value='present' name='state' id='presentCheck' class='checksimulation checker' "+pastC+"/>" +
        "<label style='float:left;line-height: 27px; ' for='pastCheck'>1982</label>" +
        "<input style='float:left;line-height: 27px; ' type='checkbox' value='past' name='state' id='pastCheck' class='checksimulation checker' "+presentC+"/>" ;

    this.domFragment.appendChild(domify(checkPeriodsContent));

    this.bindEvent();
    return this.domFragment;
};

MapRView.prototype.bindEvent=function(){
    var view = this;
    this.map.data.addListener('mouseover', function(event){
        view.mouseover(event);
    });
    this.map.data.addListener('mouseout', function(event){
        view.mouseout(event);
    });

    this.handleCheckEvent();
};

MapRView.prototype.installMapRViewCheckHanlder = function(checkHandler,domSelector){
    var view= this;
    var checks = this.domFragment.querySelectorAll(domSelector);
    var i=0;
    for (;i<checks.length;i++){
        checks[i].addEventListener("change",function(event){
            checkHandler.call(view,event);
        })
    }
};

MapRView.prototype.checkSimulation=function(event){
    var domObject=event.currentTarget;
    this.emit('mapRView.checksimulation',{checked:domObject.checked,period:domObject.value});
};

MapRView.prototype.handleCheckEvent=function(){
    this.installMapRViewCheckHanlder(this.checkSimulation,".checksimulation");
};

MapRView.prototype.refreshMapView = function() {
    google.maps.event.trigger(this.map, "resize");
    this.map.setCenter(this.mapOptions.center);
    this.map.setZoom(this.mapOptions.zoom);
    this.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
};
MapRView.prototype.updateCheckedMapData= function(jsonMapData){

    var view= this;
    this.map.data.setStyle(function(feature) {
        var period = feature.getProperty('state');
        var s="";
        if (period){
            if (jsonMapData.checkedPeriods[period]) {
                s = view.geoJSONStyles[period];
                return ({
                    visible: true,
                    fillColor: s["fillColor"],
                    strokeColor:  s["strokeColor"],
                    strokeWeight:  s["strokeWeight"],
                    fillOpacity:  s["fillOpacity"],
                    zIndex:  s["zIndex"]
                });
            } else {
                return ({
                    visible: false
                });
            }
        }else{
            s = view.geoJSONStyles["frontier"];
            return ({
                visible: true,
                fillColor: s["fillColor"],
                strokeColor:  s["strokeColor"],
                strokeWeight:  s["strokeWeight"],
                fillOpacity:  s["fillOpacity"]
            });
        }


    });
};


MapRView.prototype.mouseover = function(event) {
    var eFeature=event.feature;
    this.emit('mapview.mouseover.area',eFeature.getProperty('id'));
};

MapRView.prototype.mouseout = function(event) {
    var eFeature=event.feature;
    this.emit('mapview.mouseout.area',eFeature);
};




module.exports = MapRView;
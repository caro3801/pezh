var MapRView = require('./mapRView');
var SideView = require('./sideView');
var PageView = require("../pages/pageView");
var demoresultsTemplate = require('../../textes/solutions/mapdemoresultats.html');

var InfoAreaView = require("../areas/infoAreaView");
var Wrapper = require('./../shared/wrapper');
var WrapperGroup = require('./../shared/wrapperGroup');

var domify = require('domify');


function ResultsView(zone){

    this.mapWrapper = new Wrapper('.map-wrapper');
    this.mapWrapper.addClass("container_map");
    this.sideWrapper = new Wrapper('.side-wrapper');
    this.infoAreaWrapper = new Wrapper('.container_map_info');

    this.mapView = new MapRView(zone);
    this.sideView= new SideView();
    this.infoAreaView = new InfoAreaView();
    this.pageView =new PageView();
    this.separatorWrapper ={domElement: domify('<p style="clear:both;"></p>')};
    var wrappers = [this.mapWrapper,this.infoAreaWrapper,this.separatorWrapper,this.sideWrapper];

    this.wrapperGroup = new WrapperGroup(wrappers);
}

ResultsView.prototype.render = function(jsonData){
    this.mapWrapper.inject(this.mapView.render(jsonData.mapData));
    this.renderInfoArea();
    this.sideWrapper.inject(this.sideView.render(jsonData.dlData));
    return this.pageView.renderDOM(domify(demoresultsTemplate),this.wrapperGroup.getFragment());
};

ResultsView.prototype.refreshMapView = function() {
    this.mapView.refreshMapView();
};
ResultsView.prototype.updateMap = function (jsonMapData){
    this.mapView.updateCheckedMapData(jsonMapData);
};

ResultsView.prototype.clearInfoArea = function() {
    this.infoAreaWrapper.inject(this.infoAreaView.clear());
};

ResultsView.prototype.renderInfoArea = function(areaInfo) {
    this.infoAreaWrapper.inject(this.infoAreaView.render(areaInfo));
};
ResultsView.prototype.renderError = function() {
    this.sideWrapper.domElement.querySelector('.error').innerHTML = "Cochez au moins une zone dans la liste";
}
ResultsView.prototype.onSideView = function(event, callBack) {
    this.sideView.on(event, callBack);
};

ResultsView.prototype.onMapRView = function(event, callBack) {
    this.mapView.on(event, callBack);
};


module.exports = ResultsView;
"use strict";
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');
var doT = require('dot');
var registerTemplate= require('./../../templates/demo/registerTemplate.html');

function RegisterView(){
    this.fragment = null;
    this.template = doT.compile(registerTemplate, undefined, {});
}
RegisterView.prototype = Object.create(EventEmitter2.prototype);
RegisterView.prototype.constructor = RegisterView;

RegisterView.prototype.render = function registerViewRender(){
    this.fragment = document.createDocumentFragment();
    this.fragment.appendChild(domify(registerTemplate));
    this.bindEvent();
    return this.fragment;
};

RegisterView.prototype.submitEmail=function(e){
    if (e.preventDefault) {
        e.preventDefault();
    }
    this.emit('registerview.submit',e.currentTarget);
    return false;
};


RegisterView.prototype.installRegisterViewHandler=function(registerViewHandler, domSelector){
    var view=this;
    var form=this.fragment.querySelector(domSelector);
    if(form.attachEvent) {
        form.attachEvent("submit", function(event){
            registerViewHandler.call(view, event);
        });

    }else {
        form.addEventListener("submit", function (event) {
            registerViewHandler.call(view, event);
        });

    }

};

RegisterView.prototype.bindEvent= function(){
    this.handleSubmitEvent();
};

RegisterView.prototype.handleSubmitEvent= function(){
    this.installRegisterViewHandler(this.submitEmail,"form#registration");
};


module.exports= RegisterView;
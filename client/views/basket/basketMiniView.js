/**
 * Created by caroline on 01/05/14.
 */
"use strict";
var domify = require('domify');
function BasketMiniView() {
}

BasketMiniView.prototype.render = function basketMiniViewRender(nbItemBasket) {
    var fragment = document.createDocumentFragment();
    var domString = "<a href='/#/basket'>Panier</a> ("+nbItemBasket+") ";
    fragment.appendChild(domify(domString));
    return fragment;
};

module.exports = BasketMiniView;
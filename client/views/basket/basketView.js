/**
 * Created by caroline on 02/05/14.
 */
"use strict";
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');

function BasketView(){
    this.fragment=null;
}

BasketView.prototype = Object.create(EventEmitter2.prototype);
BasketView.prototype.constructor = BasketView;

BasketView.prototype.render = function basketViewRender(basket){
    this.fragment= document.createDocumentFragment();
    var debut="<div id='basket'>Panier</div>";
    var content = "<ul>";
        for (var i = 0 ; i<basket.items.length;i++){
            var bitem= basket.items[i];

            content+="<li>"+bitem.item.id/*"<button class='decrement' data-itemid='"+bitem.item.id+"'>-</button>"*/+" x "+bitem.quantity+/*" <button class='increment' data-itemid='"+bitem.item.id+"'>+</button>  +bitem.item.price+*/" : "+bitem.item.price*bitem.quantity+" euro <button class='trashMe' data-itemid='"+bitem.item.id+"'>suppr</button></li>";
        }
    var fin="</ul>" +
        "<p>prix total : "+basket.basketTotalPrice+"</p>" +
        "<button id='results'>Voir les résultats</button>";

    this.fragment.appendChild(domify(debut+content+fin)) ;
    this.bindEvents();
    return this.fragment;
};

BasketView.prototype.clickDelItem = function (event){
    var domObject=event.currentTarget;
    this.emit('basketView.delItem',domObject.dataset.itemid);
};
BasketView.prototype.clickAddQItem = function (event){
    var domObject=event.currentTarget;
    this.emit('basketView.addQItem',domObject.dataset.itemid);
};
BasketView.prototype.clickDelQItem = function (event){
    var domObject=event.currentTarget;
    this.emit('basketView.delQItem',domObject.dataset.itemid);
};
BasketView.prototype.clickResults = function (){
    this.emit('basketView.results');
};

BasketView.prototype.installHandlerEvents=function installHandlerEvents(basketHandler,domSelector){
    var view=this;
    var buttons=this.fragment.querySelectorAll(domSelector);
    var i=0;
    for (;i<buttons.length;i++){
        buttons[i].addEventListener("click", function(event){
            basketHandler.call(view, event);
        });
    }
};
BasketView.prototype.basketHandlerAddQItem=function basketHandlerAddQItem(){
    this.installHandlerEvents(this.clickAddQItem,".increment");

};
BasketView.prototype.basketHandlerDelQItem=function basketHandlerDelQItem(){
    this.installHandlerEvents(this.clickDelQItem,".decrement");
};
BasketView.prototype.basketHandlerDelItem=function basketHandlerDelItem(){
    this.installHandlerEvents(this.clickDelItem,".trashMe");
};

BasketView.prototype.basketHandlerResults=function basketHandlerResults(){
    this.installHandlerEvents(this.clickResults,"#results");
};
BasketView.prototype.bindEvents=function(){
    this.basketHandlerAddQItem();
    this.basketHandlerDelQItem();
    this.basketHandlerDelItem();
    this.basketHandlerResults();

};

module.exports = BasketView;
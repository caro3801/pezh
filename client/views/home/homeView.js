var domify = require('domify');
var doT = require('dot');

var descriptionTemplate=require('./../../textes/accueil.html');
var homeTemplate=require('./../../templates/accueil/homeTemplate.html');

function HomeView(){
    this.template = doT.compile(homeTemplate, undefined, {});
}

HomeView.prototype.render = function(){
    return domify(this.template(descriptionTemplate));

};


module.exports = HomeView;
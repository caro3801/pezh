var EventEmitter2 = require('eventemitter2').EventEmitter2;
var domify = require('domify');
var doT = require('dot');
var stakesTemplate = require('../../templates/accueil/stakesTabsTemplate.html');

function StakeView(){
    this.template = doT.compile(stakesTemplate, undefined, {});
}

StakeView.prototype.render = function(stakesdata){
    return domify(this.template(stakesdata));
};


module.exports = StakeView;
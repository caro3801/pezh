var domify = require('domify');
var doT = require('dot');
var sliderTemplate = require('./../../templates/accueil/sliderTemplate.html');

var Wrapper = require("../shared/wrapper");
var WrapperGroup = require("..//shared/wrapperGroup");
function SliderView(){

    this.sliderWrapper = new Wrapper(".slider-content");
    this.sliderWrapper.setId("slider-content");
    var wrappers = [this.sliderWrapper];
    this.wrapperGroup = new WrapperGroup(wrappers);
    this.template = doT.compile(sliderTemplate, undefined, {});
}

SliderView.prototype.render = function(data){
    this.sliderWrapper.inject(domify(this.template(data)));
    return this.wrapperGroup.getFragment();
};

SliderView.prototype.reload = function(){
    imageSlider.reload();
};

module.exports = SliderView;
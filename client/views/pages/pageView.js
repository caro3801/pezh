"use strict";
var doT = require('dot');
var domify = require('domify');
var Wrapper = require('../shared/wrapper');
var WrapperGroup = require('../shared/wrapperGroup');
var pageTemplate = require("../../templates/autres/pageTemplate.html");
var EventEmitter2 = require('eventemitter2').EventEmitter2;

function PageView(){
    this.cWrapper = new Wrapper('.content');

    this.wrappergroup = new WrapperGroup([this.cWrapper]);
}

PageView.prototype = Object.create(EventEmitter2.prototype);
PageView.prototype.constructor = PageView;

PageView.prototype.render = function (page){
    var pageText;
    switch (page.name) {
        //a propos
        case "valeurs":
            pageText = require('../../textes/apropos/valeurs.html');
            break;
        case "modeles":
            pageText = require('../../textes/apropos/modeles.html');
            break;
        case "partenaires":
            pageText = require('../../textes/apropos/partenaires.html');
            break;

        case "au_service_de":
            pageText = require('../../textes/apropos/servicede.html');
            break;
        //enjeux
        case "stakes":
            pageText = require('./../../textes/enjeux.html');
            break;

        //solutions
        case "map":
            pageText = require('./../../textes/solutions/map.html');
            break;

        case "forecast":
            pageText = require('./../../textes/solutions/forecast.html');
            break;

        case "city":
            pageText = require('./../../textes/solutions/city.html');
            break;

        case "eco":
            pageText = require('./../../textes/solutions/eco.html');
            break;
        case "mentions":
            pageText = require('./../../textes/mentions.html');
            break;
        case "plan":
            pageText = require('./../../textes/plan.html');
            break;

        case "contact":
            pageText = require('./../../textes/contact.html');
            pageText +=require('./../../templates/contact/contactTemplate.html');
        break;
        default :
            pageText = page;

    }
    this.tmpl=doT.compile(pageTemplate, undefined, {});

    this.cWrapper.inject( domify(this.tmpl(pageText)));

    if(page.name=="contact"){

        this.bindEvent();
    }
    return this.wrappergroup.getFragment();
};

PageView.prototype.renderDOM=function (domFirstText,domIndentText){

    this.tmpl=doT.compile(pageTemplate, undefined, {});
    this.cWrapper.inject( domify(this.tmpl("")));

    var indentDom=this.cWrapper.domElement.querySelector(".padding-text");
    indentDom.appendChild(domFirstText);
    indentDom.appendChild(domIndentText);
    return this.wrappergroup.getFragment();
};


PageView.prototype.bindEvent = function(){

    this.handleContactFormEvent();
};
PageView.prototype.handleContactFormEvent=function(){
    this.installContactFormHandler(this.submitContact,"form#contact");
};
PageView.prototype.submitContact = function(e){
    if (e.preventDefault) e.preventDefault();

    var values={last_name:e.currentTarget[0].value,
        first_name:e.currentTarget[1].value,
        email:e.currentTarget[2].value,
        company:e.currentTarget[3].value,
        object:e.currentTarget[4].value,
        content:e.currentTarget[5].value,
        news:e.currentTarget[6].checked,
        id:""};
    this.emit('contactview.contactsubmit',values);
};

PageView.prototype.installContactFormHandler=function(contactViewHandler, domSelector){
    var view=this;
    var form=this.cWrapper.domElement.querySelector(domSelector);
    if(form.attachEvent) {
        form.attachEvent("submit", function(event){
            contactViewHandler.call(view, event);
        });

    }else {
        form.addEventListener("submit", function (event) {
            contactViewHandler.call(view, event);
        });

    }

};
PageView.prototype.jumpTo=function(elem){
    var top =0;
    if (elem){
        top = document.getElementById(elem).offsetTop+240; //Getting Y of target element
    }
    window.scrollTo(0, top);

                            //Go there.
};

PageView.prototype.thanksDisabled =function(){
    var elem=this.cWrapper.domElement.querySelectorAll('input')[6];
    elem.disabled ="disabled";
    elem.style.backgroundColor ="#748490";
    elem.style.cursor ="default";
    elem.style.textDecoration ="initial";
    elem.value ="MERCI";

                            //Go there.
};


module.exports = PageView;
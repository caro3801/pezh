var XHR = require('../XHR');
var areaStore = {

};

areaStore.getAreas = function (isdemo, callBack) {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    var rem = "/areas/list";
    if (isdemo)
        rem += "/demo";
    xhr.get(rem, false);
    xhr.send(null);
    var areas = {};
    var jsonCollection = JSON.parse(xhr.jsonText);
    jsonCollection.forEach(function (elem) {
        areas[elem.id] = elem;
    });
    return areas;
};

areaStore.getArea = function (areaID) {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.get("/areas/" + areaID, false);
    xhr.send(null);

    return JSON.parse(xhr.jsonText);
};
areaStore.getSegments = function () {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.get("/search/segments", false);
    xhr.send(null);

    return JSON.parse(xhr.jsonText);
};

areaStore.saveVisu = function (data) {
    for (var key in data.areas) {
        data.selectedMuns.push(key);
    }
    var xhr = new XHR(XHR.createXMLHttpRequest());
    var rem = "/visualisation/save";
    xhr.post(rem, false);
    xhr.send(JSON.stringify(data));

    return JSON.parse(xhr.jsonText);
};

areaStore.getVisuMunList = function (visuId) {

    var xhr = new XHR(XHR.createXMLHttpRequest());

    xhr.get("/areas/visu/"+visuId, false);
    xhr.send(null);

    return JSON.parse(xhr.jsonText);
};


areaStore.getPlaces = function () {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.get("/search/places", false);
    xhr.send(null);

    return JSON.parse(xhr.jsonText);
};

module.exports = areaStore;
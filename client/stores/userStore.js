var XHR = require('../XHR');
var userStore = {

};
userStore.saveEmailNews = function (email, callback) {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.post("/news", false);
    xhr.send(JSON.stringify({mail:email,news:true}));

    return JSON.parse(xhr.jsonText);
};

userStore.login = function (email, callback) {
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.post("/login", false);
    xhr.send(JSON.stringify(email));

    return JSON.parse(xhr.jsonText);
};
userStore.saveContactForm = function(values,callback){
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.post("/contact", false);
    xhr.send(JSON.stringify(values));

    return JSON.parse(xhr.jsonText);
};
module.exports = userStore;
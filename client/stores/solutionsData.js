var solutionsD =
{

    products: [
        {

            url: "#/a_propos/modeles/hydro",
            image: "slide1.jpg",
            title: "Simulation hydrodynamique<br>des milieux humides<br>en plaine alluviale",
            desc: "Notre modèle est fiable à plus de 90% par rapport aux données piézométriques",
            plus:"En savoir plus sur le modèle ..."
        },
        {
            url: "#/enjeux/cartographier",
            image: "slide2.jpg",
            title: "<br>Cartographie des milieux humides<br>sans relevés terrain",
            desc: "Nous modélisons les échanges rivière/nappes souterrain",
            plus:"En savoir plus ..."

        },
        {
            url: "#/enjeux/prevoir",
            image: "slide3.jpg",
            title: "<br>Risques liés aux changements climatiques",
            desc: "Nous simulons les évolutions de la saturation des sols en eau",
            plus:"En savoir plus ..."
        },
        {
            url: "#/enjeux/anticiper",
            image: "slide4.jpg",
            title: "<br>Faisabilité de<br>projets d&apos;aménagement sur zones sensibles",
            desc: "Nous évaluons les impacts environnementaux, socio-éco et les compensations",
            plus:"En savoir plus ..."
        },
        {
            url: "#/enjeux/conformite",
            image: "slide5.jpg",
            title: "<br><br>Conformité des projets en milieux humides",
            desc: "Nous accompagnons le diagnostic et la mise en conformité",
            plus:"En savoir plus ..."
        },
        {
            url: "#/enjeux/sensibiliser",
            image: "slide6.jpg",
            title: "Hydriis CITY<br>La pédagogie de l&apos;aménagement du territoire",
            desc: "Nous vous aidons à sensibiliser vos publics aux enjeux de la gestion équilibrée de la ressource en eau",
            plus:"En savoir plus ..."

        }

    ]
};

module.exports = solutionsD;
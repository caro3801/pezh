var XHR = require('../XHR');
var tokml=require("tokml");
var resultStore = {

};

resultStore.getCellsFromMuns = function (muns) {

    var xhr = new XHR(XHR.createXMLHttpRequest());
    var rem = "/visualisation";
    xhr.post(rem, false);
    xhr.send(JSON.stringify({municipalities: muns}));

    return JSON.parse(xhr.jsonText);
};


resultStore.getCellsFromVisuId = function (visuId) {

    var xhr = new XHR(XHR.createXMLHttpRequest());
    var rem = "/visualisation/" + visuId;
    xhr.get(rem, false);
    xhr.send(null);


    return JSON.parse(xhr.jsonText);
};


resultStore.getJsonResultCells = function (data) {
    var json = {mapData: {cellsData:{},frontiersData:{}}, past: [], present: [],
        dlData: {

        }
    };

    var stateObj = {
        "past": {
            features: [], dlData: []
        },
        "present": {
            features: [], dlData: []
        }
    };
    var state=["past","present"];


    json.mapData.cellsData = {
        type: "FeatureCollection",
        features: []
    };
    var frontiersArea={};
    json.mapData.frontiersData = {
        type: "FeatureCollection",
        features: []
    };
    var feature = {};
    var frontier = {};

    for (var i = 0; i < data.length; i++) {

        feature =
        {
            "type": "Feature",
            "properties": {
                "id": data[i].municipality_id,
                "name": data[i].name,
                "department": data[i].department,
                "country": data[i].country,
                "simu_id": data[i].simu_id,
                "state": data[i].simu_state,
                "display": data[i].simu_state
            },
            "geometry": JSON.parse(data[i].geom)
        };
        frontier = {
            "type": "Feature",
            "properties": {
                "id": data[i].municipality_id,
                "name": data[i].name,
                "display":"frontier"
            },
            "geometry": JSON.parse(data[i].frontier)
        };

        stateObj[data[i].simu_state].features.push(feature);
        if (!frontiersArea[data[i].municipality_id]){
            frontiersArea[data[i].municipality_id]=true;
            json.mapData.frontiersData.features.push(frontier);
        }
        if (! json.dlData[data[i].municipality_id]){
            json.dlData[data[i].municipality_id] = {
                "id":data[i].municipality_id,
                "name":data[i].name,
                "department": data[i].department,
                "country": data[i].country,
                "past":{},
                "present":{}
            };
        }
        json.dlData[feature.properties.id][feature.properties.state] = { json: feature};

    }

    json.mapData.cellsData.features = stateObj['past'].features.concat(stateObj['present'].features);

    return json;
};


resultStore.createDataFromForm = function createDataFromForm(muns,format,data){

    var past={
        type: "FeatureCollection",
        features: []
    };
    var present={
        type: "FeatureCollection",
        features: []
    };
    var frontiers={
        type: "FeatureCollection",
        features: []
    };
    function keepOnlyMunsIdFeatures(feature) {
        function isInArray(value, array) {
            return array.indexOf(value) > -1;
        }
        return isInArray(feature.properties.id+"", muns);
    }

    var cells = data.cellsData.features.filter(keepOnlyMunsIdFeatures);
    var frontiersF = data.frontiersData.features.filter(keepOnlyMunsIdFeatures);

    frontiers.features=frontiersF;

    for (var i=0;i<cells.length;i++){
        if (cells[i].properties.state == "past"){
            past.features.push(cells[i]);
        }else{
            present.features.push(cells[i]);
        }
    }

    if (format =="kml"){
        frontiers=tokml(frontiers);
        past=tokml(past);
        present=tokml(present);

    }else{
        past=JSON.stringify(past);
        present=JSON.stringify(present);
        frontiers=JSON.stringify(frontiers);
    }

    return {format:format,"frontiers":frontiers,"past":past,"present":present};
};


module.exports = resultStore;
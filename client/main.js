/**
 * Created by caroline on 30/04/14.
 */
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var AreasPresenter = require('./presenters/areasPresenter');
var HeaderPresenter = require('./presenters/headerPresenter');
//var BasketPresenter = require('./presenters/basketPresenter');
var HomePresenter = require('./presenters/homePresenter');
var FooterPresenter = require('./presenters/footerPresenter');
var PagePresenter = require('./presenters/pagePresenter');
var ResultsPresenter = require('./presenters/resultsPresenter');
var Wrapper = require('./views/shared/wrapper');
function router(hash) {
    var menuWrapper = new Wrapper(".header");
    var contentWrapper = new Wrapper('.main');
    var footerWrapper = new Wrapper('.footer');

    this.bus=new EventEmitter2();
    var withSlider=(hash.length===0 || /accueil/.test(hash));

    new HeaderPresenter(this.bus,menuWrapper,withSlider);

    var page={name:"",anchor:""};
    var activePage="accueil";
    switch (true){
        case hash.length===0:
            activePage="accueil";
            new HomePresenter(this.bus,contentWrapper);
            break;
        case /accueil/.test(hash):
            activePage="accueil";
            new HomePresenter(this.bus,contentWrapper);
            break;
        case /city/.test(hash):
            new HomePresenter(this.bus,contentWrapper);
            break;
        case /solutions/.test(hash):
            activePage="solutions";
            switch(true) {
                case /map_demo/.test(hash):
                    var visuid=hash.split("/")[3];
                    new AreasPresenter(this.bus, contentWrapper,true,visuid);
                    break;
                case /map/.test(hash):
                    page.name="map";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /forecast/.test(hash):
                    page.name="forecast";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /eco/.test(hash):
                    page.name="eco";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /city/.test(hash):
                    page.name="city";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
            }
            break;
        case /visualisation/.test(hash):
            activePage="solutions";
            var visuid=hash.split("/")[2];
            new ResultsPresenter(this.bus,contentWrapper, visuid);
            break;
        case /a_propos/.test(hash):
            activePage="apropos";
            switch(true) {
                case /valeurs/.test(hash):
                    page.name="valeurs";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /modeles/.test(hash):
                    page.name="modeles";
                    page.anchor=hash.split("/")[3];
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /partenaires/.test(hash):
                    page.name="partenaires";
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;
                case /au_service_de/.test(hash):
                    page.name="au_service_de";
                    page.anchor=hash.split("/")[3];
                    new PagePresenter(this.bus, contentWrapper, page);
                    break;

            }
            break;
        case /enjeux/.test(hash):
            activePage="enjeux";
            page.name="stakes";
            page.anchor=hash.split("/")[2];
            new PagePresenter(this.bus, contentWrapper, page);
            break;
        case /contact/.test(hash):
            activePage="contact";
            page.name="contact";
            new PagePresenter(this.bus,contentWrapper,page);
            break;
        case /plan/.test(hash):
            activePage="plan";
            page.name="plan";
            new PagePresenter(this.bus,contentWrapper,page);
            break;
        case /mentions_legales/.test(hash):
            activePage="mentions";
            page.name="mentions";
            new PagePresenter(this.bus,contentWrapper,page);
            break;

        /*case /basket/.test(hash):
            new BasketPresenter(this.bus,contentWrapper);
            break;*/
        default :
            activePage="accueil";
            new HomePresenter(this.bus,contentWrapper);


    }

    new FooterPresenter(this.bus,footerWrapper,activePage);

 }

var hashEvent;
document.addEventListener("DOMContentLoaded", function(event) {
    window.addEventListener("hashchange", function(hashChangeEvent) {
        var hash = hashChangeEvent.target.location.hash;
        router(hash);
    });
    var hash = window.location.hash;
    router(hash);
});
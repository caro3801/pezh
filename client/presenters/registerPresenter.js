/**
 * Created by caroline on 06/06/2014.
 */

var RegisterView = require("../views/register/registerView");

function RegisterPresenter(bus,wrapper){
    this.bus=bus;
    this.registerView = new RegisterView();
    this.registerListener();
    wrapper.inject(this.registerView.render(this.results));

}

RegisterPresenter.prototype.registerListener = function(){
    var presenter = this;
    this.registerView.on("registerview.submit",function(registrationData){
        //presenter.userStore.saveUser(registrationData);

    });

};

module.exports = RegisterPresenter;

var PageView = require("../views/pages/pageView");
var userStore = require("../stores/userStore");
function PagePresenter(bus, wrapper, page) {
    this.bus = bus;
    this.wrapper = wrapper;
    this.userStore=userStore;
    this.pageView = new PageView();

    this.wrapper.inject(this.pageView.render(page));

    this.pageViewListener();
    this.pageView.jumpTo(page.anchor);
}


PagePresenter.prototype.pageViewListener = function (){
    var presenter = this;
    this.pageView.on('contactview.contactsubmit',function(formdata){
        presenter.saveContactForm(formdata);

    });
};

PagePresenter.prototype.saveContactForm = function(formdata){
    this.pageView.thanksDisabled();
    this.userStore.saveContactForm(formdata);
};

module.exports = PagePresenter;
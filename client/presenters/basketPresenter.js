/**
 * Created by caroline on 01/05/14.
 */
var BasketView = require("../views/basket/basketView");
var basketModel = require("../models/basketModel");
var basketStore = require("../models/basketStore");
var areaStore = require("../stores/areaStore");

function BasketPresenter(bus,wrapper) {
    this.basket = basketModel;
    this.basketStore = basketStore;
    this.bus=bus;
    this.wrapper = wrapper;
    this.areaStore= areaStore;
    this.basketView = new BasketView();
    this.wrapper.inject(this.basketView.render(this.basket));
    this.basketListener();
}

BasketPresenter.prototype.basketListener= function(){
    var presenter=this;

    this.basketView.on("basketView.delItem",function(areaId){
        var item = presenter.areaStore.getArea(areaId);
        presenter.basket.deleteItem(item);
        presenter.wrapper.inject(presenter.basketView.render(presenter.basket));
        presenter.bus.emit("basketView.updateMiniBasket");
    });
    this.basketView.on("basketView.addQItem",function(areaId){
        var item = presenter.areaStore.getArea(areaId);
        presenter.basket.addItem(item);
        presenter.wrapper.inject(presenter.basketView.render(presenter.basket));
    });
    this.basketView.on("basketView.delQItem",function(areaId){
        var item = presenter.areaStore.getArea(areaId);
        presenter.basket.decrementItem(item);
        presenter.wrapper.inject(presenter.basketView.render(presenter.basket));
        presenter.bus.emit("basketView.updateMiniBasket");
    });
    this.basketView.on("basketView.results",function(){
        var id=presenter.saveBasket(presenter.basket);
        window.location.hash='#/results/'+id;

        window.triggerEvent('hashchange');
    });
};

BasketPresenter.prototype.saveBasket = function saveBasket(basket){
    return this.basketStore.saveBasket(basket);
}

module.exports = BasketPresenter;
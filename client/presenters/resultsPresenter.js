/**
 * Created by caroline on 06/06/2014.
 */

var ResultsView = require("../views/visualisation/resultsView");
var resultStore = require('../stores/resultStore');
var JSZip = require ('jszip');
var zone = {
    latitude: 43.9281953,
    longitude: 1.2549749,
    zoom: 10
};
function ResultsPresenter(bus, wrapper, visuId) {
    this.bus = bus;
    this.wrapper = wrapper;
    this.visuId = visuId;

    this.resultStore = resultStore;

    this.resultsCells = this.resultStore.getCellsFromVisuId(visuId);

    this.jsonData = this.resultStore.getJsonResultCells(this.resultsCells);
    this.jsonData.mapData.checkedPeriods = {"past":true,"present":true};
    this.resultsView = new ResultsView(zone);

    this.wrapper.inject(this.resultsView.render(this.jsonData));
    this.resultsViewListener();

    this.resultsView.refreshMapView(zone);

}

ResultsPresenter.prototype.resultsViewListener = function () {
    var presenter = this;
    this.resultsView.onSideView('sideview.downloadSubmit', function (domObj) {
        var oneChecked=false;
        for (var i=0;i<domObj.length-2;i++){
            if(domObj[i].checked){
                oneChecked=true;
                break;
            }
        }
        if (oneChecked){
            presenter.createZipDownload(domObj);
        }else{
            presenter.resultsView.renderError();
        }
    });
    this.resultsView.onMapRView('mapRView.checksimulation', function (periodCheck) {
        presenter.jsonData.mapData.checkedPeriods[periodCheck.period]=periodCheck.checked;
        presenter.resultsView.updateMap(presenter.jsonData.mapData);
    });
    this.resultsView.onMapRView('mapview.mouseover.area',function(areaId){
        var area=presenter.jsonData.dlData[areaId];
        var infos={name:area.name,departement:area.department,pays:area.country};
        presenter.resultsView.renderInfoArea(infos);
    });
    this.resultsView.onMapRView('mapview.mouseout.area',function(areaId){
        presenter.resultsView.clearInfoArea();
    });
};

ResultsPresenter.prototype.createZipDownload=function createZipDownload(domObj){
    function isChecked(elem){
        return elem.checked==true;
    }
    var filteredCheckedMuns= Array.prototype.slice.call(domObj.querySelectorAll('input.complete')).filter(isChecked);
    var muns=[];
    for (var i = 0;i<filteredCheckedMuns.length;i++){
        muns[i]=filteredCheckedMuns[i].value;
    }
    var format=domObj.querySelector(".format").value;
    var newData=this.resultStore.createDataFromForm(muns, format,this.jsonData.mapData);
    var zip = new JSZip();
    var data=zip.folder("HMD");
    data.file("HMD_1982."+newData.format,newData.past);
    data.file("HMD_2013."+newData.format,newData.present);
    data.file("HMD_Frontieres."+newData.format,newData.frontiers);
    var content = zip.generate({type:"blob"});

    var saveAs = require ('filesaver.js');
    saveAs(content, "Hydriis_Map_DEMO.zip");
};

module.exports = ResultsPresenter;
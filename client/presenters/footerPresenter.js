var FooterView = require("../views/shared/footerView");


function FooterPresenter(bus,wrapper,activePage) {
    this.bus=bus;
    this.wrapper = wrapper;
    this.footerView = new FooterView();
    this.active = activePage;
    this.activeObject = {
        accueil:false,
        solutions:false,
        apropos:false,
        enjeux:false,
        contact:false,
        plan:false,
        mention:false
    };
    this.activeObject[this.active]=true;
    this.wrapper.inject(this.footerView.render(this.activeObject));
}



module.exports = FooterPresenter;
var headerView = require("../views/shared/headerView");
var FooterView = require("../views/shared/footerView");
var userStore = require("../stores/userStore");
var data= require('../stores/solutionsData');
var domify = require('domify');
function HeaderPresenter(bus, wrapper,withSlider){
    this.bus=bus;
    this.wrapper = wrapper;
    this.userStore=userStore;
    this.headerView = new headerView(data,withSlider);
    if (withSlider){
        this.wrapper.domElement.style.height = "475px";

        this.wrapper.inject(this.headerView.renderSlider());
        this.wrapper.append(this.headerView.render(withSlider));
        this.wrapper.append(domify('<div id="header-border"  ></div>'));
        this.reloadSlider();
    }else{
        this.wrapper.domElement.style.height = "240px";
        this.wrapper.inject(this.headerView.renderImage());
        this.wrapper.append(this.headerView.render(withSlider));
        this.wrapper.append(domify( '<div id="header-border" style="top:230px;"></div>'));
    }
    this.headerViewListener();
}

HeaderPresenter.prototype.headerViewListener = function (){
    var presenter = this;
    this.headerView.onMenuView('menuview.newssubmit',function(value){
        presenter.saveEmailNews(value);
    });
};
HeaderPresenter.prototype.saveEmailNews = function(email){
    this.userStore.saveEmailNews(email);
    this.headerView.thanksNewsDisabled();
};

HeaderPresenter.prototype.reloadSlider = function (){
    this.headerView.sliderView.reload();
};

module.exports = HeaderPresenter;
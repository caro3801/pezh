var areaStore = require("../stores/areaStore");
var userStore = require("../stores/userStore");
var AreasView = require("../views/areas/areasView");
var uuid= require ('node-uuid');
var basket = require("../models/basketModel");

var zone = {
    latitude:43.9281953,
    longitude:1.2549749,
    zoom:10
};

function AreasPresenter(bus, wrapper,isdemo,visuid) {
    this.bus=bus;
    this.selectedArea = {};

    this.visuId=uuid.v1();
    this.areaStore = areaStore;
    this.userStore = userStore;
    this.areas = this.areaStore.getAreas(isdemo);
    this.segments = this.areaStore.getSegments(this.areas);
    this.places = this.areaStore.getPlaces();
    this.data={map:this.areas,searchplaces:{places:this.places,segments:this.segments}};

    this.areasView = new AreasView(zone,this.visuId);

    this.basket = basket;
    this.areasViewListener();

    this.wrapper=wrapper;

    this.wrapper.inject(this.areasView.render(this.data));

    this.areasView.refreshMapView(zone);
    if (visuid && visuid!=""){
        this.initVisuAreas(visuid);
    }
    this.initBasketAreas(this.basket.items);
    window.scrollTo(0,0);
}

AreasPresenter.prototype.initVisuAreas=function(visuid){
    var muns=this.areaStore.getVisuMunList(visuid);
    for( var i=0;i< muns.length ;i++){
        var areaId=muns[i];
        this.selectedArea[areaId]=this.areas[areaId];
        this.areasView.renderArea({id:areaId, selected:true});
    }
    this.areasView.renderList(this.selectedArea);
};
AreasPresenter.prototype.initBasketAreas=function(basketareas){
    for( var i=0;i< basketareas.length ;i++){
        var areaId=basketareas[i].item.id;
        this.selectedArea[areaId]=this.areas[areaId];
        this.areasView.renderArea({id:areaId, selected:true});
    }
    this.areasView.renderList(this.selectedArea);
};
AreasPresenter.prototype.areasViewListener = function() {
    var presenter = this;
    this.areasView.onMapView('mapview.click.area',function(areaId){
        var selected = false;
        if(presenter.selectedArea[areaId]) {
            delete presenter.selectedArea[areaId];
            presenter.bus.emit("areaspresenter.delFrombasket",areaId);
        } else {
            presenter.selectedArea[areaId] = presenter.areas[areaId];
            selected =true;
            presenter.bus.emit("areaspresenter.addTobasket",areaId);
        }
        presenter.areasView.renderArea({id:areaId, selected:selected});
        presenter.areasView.renderList(presenter.selectedArea);
    });
    this.areasView.onMapView('mapview.mouseover.area',function(areaId){
        var area=presenter.areas[areaId];
        var infos={name:area.name,departement:area.department,pays:area.country};
        presenter.areasView.renderInfoArea(infos);
    });
    this.areasView.onMapView('mapview.mouseout.area',function(areaId){
        presenter.areasView.renderArea({id:areaId, selected:presenter.selectedArea[areaId]});
        presenter.areasView.clearInfoArea();
    });
    this.areasView.onListView('listareaview.delete',function(areaId){
        delete presenter.selectedArea[areaId];

        presenter.bus.emit("areaspresenter.delFrombasket",areaId);
        presenter.areasView.renderArea({id:areaId, selected:false});
        presenter.areasView.renderList(presenter.selectedArea);
    });
    this.areasView.onListView('listareaview.results',function(selectedMuns){
        var visu={visu:{uuid:presenter.visuId,municipalities:selectedMuns}};
        presenter.areaStore.saveVisu(visu);
    });

    this.areasView.onSearchView('searchView.submit',function(areaName){
        function filterPlace(elem){
            return elem.name ==areaName;
        }
        var place = presenter.places.filter(filterPlace)[0];

        var center=JSON.parse(place.center_point);
        var zoom=12;
        if (place.center_point) {
            center =   {latitude:center.coordinates[1],longitude:center.coordinates[0]};
        }else{

            switch (place.code){
                case 31:
                    center =  {latitude:"43.4010462",longitude:"1.1353020000000242"};
                    zoom=10;
                    break;
                case 82:
                    center =  {latitude:"43.9264401",longitude:"1.9881527000000006"};
                    zoom=10;
                    break;
            }
        }

        presenter.areasView.centerMapView(center);
        presenter.areasView.zoomMapView(zoom);
    });

    this.areasView.onSearchView('searchView.segmentclick',function(segmentname){

        var seg=presenter.segments[0];//FIX
        var centerjson=JSON.parse(seg.center_point);
        var center={latitude:centerjson.coordinates[1],longitude:centerjson.coordinates[0]};
        presenter.areasView.centerMapView(center);

        presenter.areasView.zoomMapView(11);
    });

    this.areasView.onRegisterView('registerview.submit',function(email){
        if(Object.keys(presenter.selectedArea).length>0){
            presenter.userStore.login({mail:email.mail.value});
            presenter.areaStore.saveVisu({email:email.mail.value,visuId:presenter.visuId,url:window.location.origin+"/#/visualisation/"+presenter.visuId,areas: presenter.selectedArea,selectedMuns:[]});

            window.location.hash = "/visualisation/"+presenter.visuId;
            presenter.bus.emit('areasview.registrationsubmit',email);
        }
    });

};

module.exports = AreasPresenter;
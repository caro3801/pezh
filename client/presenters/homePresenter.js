
var HomeView = require("../views/home/homeView");


function HomePresenter(bus,wrapper) {
    this.bus=bus;
    this.wrapper = wrapper;
    this.homeView = new HomeView();
    this.wrapper.inject(this.homeView.render());
}


module.exports = HomePresenter;
var SliderView = require("../views/home/sliderView");
var solutions=require("../stores/solutionsData");

function SliderPresenter(bus, wrapper){
    this.bus=bus;
    this.wrapper = wrapper;
    this.sliderView = new SliderView();
    this.wrapper.domElement.appendChild(this.sliderView.render(solutions));
    this.sliderView.reload();
}

module.exports = SliderPresenter;
var express = require('express');
var restrictedAccess = require('../session');
var router = express.Router();
var emailer = require('../transporter');

var Db = require('../db');

/* POST mun cells. */
    router.post('/', restrictedAccess, function(req, res) {
    var municipalities=req.body.municipalities;
    var db=new Db();

    db.getMunicipalitiesCells(municipalities, function (data) {

        res.send(data);
    });

});


router.post('/save', restrictedAccess, function(req, res) {
    var data = req.body;
    var db=new Db();

    emailer.resultForm(data);
    db.saveVisu(data.visuId,data.selectedMuns, function (data) {
        res.send(data);
    });

});

router.get("/:visuId", restrictedAccess,function(req,res){
   var id=req.params.visuId;
   var db= new Db();
    db.getCellsFromVisuId(id,function(data){
        res.send(data);
    });
});

module.exports = router;
var express = require('express');
var router = express.Router();
var restrictedAccess = require('../session');
var Db= require('../db');


router.get('/segments',function(req,res){
    var db = new Db();
    db.getSegments(function(segments){
        res.send(segments);
    });
});

router.get('/places',function(req,res){
    var db = new Db();
    db.getPlaces(function(places){
        res.send(places);
    });
});

module.exports = router;
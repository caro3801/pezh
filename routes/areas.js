var express = require('express');
var router = express.Router();
//var restrictedAccess = require('../session');

var Db= require('../db');

/* GET area listing. */
router.get('/list/:demo',  function(req, res) {
    var db = new Db();
    var isdemo = (req.params.demo == "demo") ;
    db.getMunicipalities(isdemo,function(cities){
        res.send(cities);
    });
});
router.get('/visu/:id',  function(req, res) {
    var db = new Db();

    db.getMunicipalityListFromVisuId(req.params.id,function(areas){
        res.send(areas);
    });

});
router.get('/:id',  function(req, res) {
    var db = new Db();
    if (!parseInt(req.params.id)){
        res.send("bad data not id integer")
    }else {
        db.getMunicipalityById(req.params.id,function(city){
            res.send(city);
        });
    }
});





module.exports = router;
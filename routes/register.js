/**
 * Created by caroline on 06/06/2014.
 */
var express = require('express');
var router = express.Router();
var emailer= require("../transporter");

var Db = require('../db');

router.post('/login', function (req, res, next) {
    var nflag='false';
    if(req.body.news){
        nflag='true';
    }
    var user={email:req.body.mail,news:nflag};
    req.session.email=user.email;
    var nDays = 300*24*60*60*1000;
    req.session.cookie.expires = new Date(Date.now() + nDays);
    req.session.cookie.maxAge = nDays;
    var db=new Db();
    db.userExist(user.email,function(id){
         if(id){

             user.id=id.id;
             db.updateUser(user);
         }else{
             db.storeUser(user,function(id2){

                 user.id=id2;
             });
         }
    });

    //res.cookie();
    res.send({session:'ok'});
});

router.post('/news', function (req, res, next) {
    var user={email:req.body.mail,news:req.body.news,id:""};

    var db=new Db();
    db.userExist(user.email,function(id){
        if(id){
            user.id=id.id;
            db.updateUser(user);
        }else{
            db.storeUser(user,function(id2){

                user.id=id2;
            });
        }
    });
    res.send({news:'ok'});
});

router.post('/contact', function (req, res, next) {

    var db=new Db();
    var data=req.body;
    emailer.contactForm(data);
    db.userExist(data.email,function(id){
        if(id){
            data.id=id.id;
            db.updateUser(data);
        }else{
            db.storeCompleteUser(data,function(id2){
                data.id=id2;
           });
        }
        db.storeContact(data);
    });
    res.send({contact:'ok'});
});
module.exports = router;
var express = require('express');
var router = express.Router();
var Db = require('../db');

router.post('/save', function (req, res, next) {
    var db = new Db();
    db.saveBasket(req.body.basket , function (id) {
        res.send(id);
    });

});

router.get('/:idBasket/municipalities', function (req, res, next) {
    var db = new Db();
    db.getBasketMunicipalities(req.params.idBasket , function (id) {
        res.send(id);
    });

});



module.exports = router;
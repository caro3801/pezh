var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'info@acceptablesavenirs.eu',
        pass: 'inaa2468'
    }
});

var emailer ={
    user:'info@acceptablesavenirs.eu'
} ;
emailer.contactForm =function contactForm(data){
    var d = new Date();

    var textCopie="" +
        "* Copie du message envoyé depuis le formulaire de contact du site Hydriss *\n" +
        "Envoyé le : " +d.toString()+
        "\nInformations de l'envoyeur :\n" +
        "Email : "+data.email+"\n" +
        "Nom : "+data.last_name+"\n" +
        "Prenom : "+data.first_name+"\n" +
        "Société : "+data.company+"\n" +
        "Contenu du message : \n" +
        ""+data.content+"\n\n" +
        "";

    transporter.sendMail({
        from:emailer.user,
        to: data.email,
        subject:"[HYDRIIS Contact] "+data.object,
        text:textCopie
    });

};
emailer.resultForm =function resultForm(data){
    var text="" +
     "Bonjour, merci d'avoir testé notre démo MAP HYDRIIS. \n" +
     "Veuillez trouver l'adresse de la simulation que vous avez effectuée : "+data.url +
     "\nVous pourrez y télécharger les fichiers générés de simulation de 1982 et 2013 pour les villes suivantes :\n ";
    var tmp=[];
    for (var key in data.areas){
        tmp.push(data.areas[key].name);
    }
     text+=tmp.join() +". \n" +
     "N'hesitez pas à nous contacter pour tout renseignement. \n\n" +
     "Cordialement,\n" +
     "L'équipe du site HYDRIIS by Acceptables Avenirs\n\n" +
         "N.B. : si vous supprimez vos cookies, le lien ne fonctionnera plus, il faudra de nouveau sélectionner les zones prédéfinies.";

    transporter.sendMail({
        from:emailer.user,
        to:data.email,
        subject:"[HYDRIIS MAP DEMO] Résultats",
        text:text

    });
};

module.exports = emailer;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');

//routes
var areas = require('./routes/areas');
var visualisation = require('./routes/visualisation');
var contact = require('./routes/contact');
var city = require('./routes/city');
//var baskets = require('./routes/baskets');
var register = require('./routes/register');
var search = require('./routes/search');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname+'/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({secret: '1234567890QWERTY',"proxy": true,
    saveUninitialized: true,
    resave: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', register);
app.use('/visualisation', visualisation);
app.use('/search', search);
app.use('/contact', contact);
//app.use('/baskets', baskets);
app.use('/areas', areas);
app.use('/city', city);
app.use('/', function(req, res) {
    res.render('index');
});



/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

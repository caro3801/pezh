var Db = require('./db');
var restrictedAccess =function (req, res, next) {
        if (req.session.email) {
            next();
        } else {
            res.status(302);
            if (/visualisation/.test(req.originalUrl)){

                var db = new Db();
                var uuid = req.originalUrl.split('/')[2];
                db.getMunicipalityListFromVisuId(uuid,function(areas){
                    res.send({url:'/#/solutions/map_demo/'+uuid});
                });
            }
            else{
                res.send({url:'/#/solutions/map_demo'});
            }
        }
    };

module.exports = restrictedAccess;
#!/bin/env node
var debug = require('debug')('my-application');
var app = require('./app');
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080; 
var ip =  process.env.IP || process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
app.set('port', port);

var server = app.listen(app.get('port'), ip,function() {
  debug('Express server listening on port ' + server.address().port);
});

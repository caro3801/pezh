module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        srcFile:['client/**/*.js','client/**/*.html'],
        browserify: {
            dist: {
                files: {
                    'public/javascripts/front.js': ['client/main.js']
                },
                options: {
                        debug: false,
                        transform: ['partialify']
                }
            }
        },
        watch: {
            dev:{
                files: ['<%= srcFile %>'],
                tasks: ['browserify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', ['browserify', 'watch:dev']);

};
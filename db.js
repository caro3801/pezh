var pg = require('pg');
var config = require('config');
function Db(){

    this.conString = config.pg_config+"/"+config.table_name;
    //console.log(this.conString);
}

/*------------------*/
Db.prototype.getMunicipalities=function getMunicipalities(isdemo,callback){

    var sql="" +
        "SELECT id, name, zip_code, department, region, country, price, intersection frontier,ST_AsGeoJson(center_point) center_point " +
        " FROM municipality,municipalitycells" +
        " WHERE visu_flag='true' and municipality.id=municipalitycells.municipality_id";
    if (isdemo)
        sql+= " AND price=0.0";

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            callback(result.rows);
        });
    });
};


Db.prototype.getMunicipalityById=function getMunicipalityById(id,callback){

    var sql="" +
        "SELECT id, name, zip_code, department, region, country, price, intersection frontier,ST_AsGeoJson(center_point) center_point" +
        " FROM municipality,municipalitycells" +
        " WHERE municipality.id="+id+" and municipality.id=municipalitycells.municipality_id ";

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            callback(result.rows[0]);
        });
    });
};


Db.prototype.getMunicipalitiesCells = function getMunicipalitiesCells(muns,callback){

    var sql="" +
        "SELECT name, municipality_id,department,country, simu_id, simu_state, simu_len, simu_start_date, simu_end_date,  geom, intersection frontier,ST_AsGeoJson(center_point) center_point " +
        " FROM municipality,municipalitycells" +
         " WHERE municipality.id = municipalitycells.municipality_id " ;
    for (var i=0;i<muns.length;i++){

        if (i==0)
            sql+= " AND (";
        else
            sql+= " OR ";

        sql+= " municipality_id="+muns[i]+"  ";
        if (i==muns.length-1)
            sql+= " ) ";



    }
    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            callback(result.rows);
        });
    });
};

Db.prototype.saveVisu=function saveVisu(uuid,muns,callback){
    var sql="" +
        "INSERT INTO visu " +
        " VALUES ('"+uuid+"','"+JSON.stringify(muns)+"') " +
        " RETURNING visu_uuid ";
    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            callback(result.rows[0]);
        });
    });
};


Db.prototype.getCellsFromVisuId=function getCellsFromVisuId(uuid,callback){

    var sql="" +
        "SELECT municipality_list" +
        " FROM visu" +
        " WHERE visu_uuid='"+uuid+"'" ;
    var that=this;
    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            var muns=result.rows[0].municipality_list;
            that.getMunicipalitiesCells(muns,callback); //TODO municipality list doit etre un array cf fonction au dessus

        });
    });
};

Db.prototype.getMunicipalityListFromVisuId=function getMunicipalityListFromVisuId(uuid,callback){

    var sql="" +
        "SELECT municipality_list" +
        " FROM visu" +
        " WHERE visu_uuid='"+uuid+"'" ;

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            callback((result.rows[0])?result.rows[0].municipality_list:[]);

        });
    });
};


Db.prototype.storeUser=function storeUser(user,callback){
    var sql="" +
        "INSERT INTO client (email,news) VALUES ('"+user.email+"','"+user.news+"') " ;

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            console.log("store user");
            callback(result.rows[0]);

        });
    });
};

Db.prototype.storeCompleteUser=function storeCompleteUser(user,callback){
    var sql="" +
        "INSERT INTO client (last_name,first_name,email,company,news) VALUES (" +
        "'"+user.last_name+"' ," +
        "'"+user.first_name+"'," +
        "'"+user.email+"'     ," +
        "'"+user.company+"'   ," +
        "'"+user.news+"'       " +
        ") RETURNING id" ;

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            callback(result.rows[0].id);

        });
    });
};
Db.prototype.updateUser=function updateUser(values){
    var sql="update client " +
        "set " +
        "last_name ='"+values.last_name+"' ," +
        "first_name ='"+values.first_name+"' ," +
        "email ='"+values.email+"' ," +
        "company ='"+values.company+"' ," +
        "news ='"+values.news+"' " +
        "where id="+values.id+" ";

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

        });
    });
};
Db.prototype.storeContact=function storeContact(values){
    console.log(values);
    var sql="" +
        "INSERT INTO contact (client_id,client_email,date,time,object,content) VALUES (" +
        ""+values.id+"      ," +
        "'"+values.email+"' ," +
        "current_date       ," +
        "current_time       ," +
        "'"+values.object+"'," +
        "'"+values.content+"') " ;

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }


        });
    });
};

Db.prototype.userExist=function userExist(email,callback){
    var sql="select id from client where email='"+email+"'";
    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }
            callback(result.rows[0]);
        });
    });
};


Db.prototype.getSegments=function getSegments(callback){
    var sql="select distinct segment.name , st_asgeojson(st_centroid(segment.geom)) center_point, st_asgeojson(box2d(segment.geom)) bbox" +
        " from  segment,municipality " +
        "where segment.name=municipality.segment_name and municipality.price=0.0";

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            callback(result.rows);
        });
    });
};


Db.prototype.getPlaces = function getPlaces(callback){

    var sql="select name, code, type, st_asgeojson(center_point) center_point from searchbox";

    pg.connect(this.conString, function(err, client, done) {
        if(err) {
            return console.error('error fetching client from pool', err);
        }
        client.query(sql,[],function(err, result) {

            //call `done()` to release the client back to the pool
            done();

            if(err) {
                return console.error('error running query', err);
            }

            callback(result.rows);
        });
    });
} ;

module.exports = Db;